import React from 'react';
import { Route, Routes } from 'react-router-dom';
import styled from 'styled-components';

import { WIDTH_BREAKPOINT } from './css.constants';

import Sidebar from './components/Sidebar';

import DiscoveredCommands from './components/DiscoveredCommands';
import Abilities from './components/Abilities';
import PlannerPage from './pages/PlannerPage';
import RecipeBookPage from './pages/RecipeBookPage';
import Welcome from './pages/Welcome';

const Container = styled.div`
  display: flex;
  height: 100vh;
  font-size: 1rem;
  text-align: center;
  width: 100vw;

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    h3 {
      display: none;
    }
  }
`;

const MainView = styled.div`
  height: 100%;
  flex: 1;

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    width: 100%;
  };
`;

const App = () => (
  <Container>
    <Sidebar />
    <MainView>
      <Routes>
        <Route path="/abilities" element={<Abilities />} />
        <Route path="/discovered" element={<DiscoveredCommands />} />
        <Route path="/planner" element={<PlannerPage />} />
        <Route path="/recipes" element={<RecipeBookPage />} />
        <Route path="/" element={<Welcome />} />
      </Routes>
    </MainView>
  </Container>
);

export default React.memo(App);
