/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { configureStore } from '@reduxjs/toolkit';

import { saveState, loadState } from './localStorage';
import reducer, { initialState } from './reducer';

const persistedState = {
  ...initialState,
  ...loadState(),
};
const store = configureStore({
  preloadedState: persistedState,
  reducer,
});

store.subscribe(() => {
  setTimeout(() => saveState(store.getState()), 10);
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
