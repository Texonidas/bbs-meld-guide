/* eslint-disable sort-keys */
import { t } from '@lingui/macro';

import rawData from './data.json';

export const abilityTypes = {
  Prizes: t`Prizes`,
  Stats: t`Stats`,
  Support: t`Support`,
};

export type AbilityType = keyof typeof abilityTypes;

export const commandTypes = {
  Attack: t`Attack`,
  Magic: t`Magic`,
  Movement: t`Movement`,
  Defence: t`Defence`,
  Reprisal: t`Reprisal`,
  Shotlock: t`Shotlock`,
};

export type CommandType = keyof typeof commandTypes;

export const crystals = {
  Shimmering: t`Shimmering`,
  Fleeting: t`Fleeting`,
  Pulsing: t`Pulsing`,
  Wellspring: t`Wellspring`,
  Soothing: t`Soothing`,
  Hungry: t`Hungry`,
  Abounding: t`Abounding`,
};

export type Crystal = keyof typeof crystals;

export const characters = {
  Terra: t`Terra`,
  Ventus: t`Ventus`,
  Aqua: t`Aqua`,
};

export type Character = keyof typeof characters;

// eslint-disable-next-line no-unused-vars
export type RecipeType = { [key in Crystal | 'type']: string };

export const recipeTypes = rawData.recipeTypes
  .reduce<Record<string, RecipeType>>((acc, recipeType) => ({
    ...acc,
    [recipeType.type]: {
      ...recipeType,
    } as RecipeType,
  }), {});

export interface Ability {
  crystal?: Crystal;
  description: string;
  max: number;
  name: string;
  recipeTypes: string[];
  type: AbilityType;
}

export const abilities = rawData.abilities
  .reduce<Record<string, Ability>>((acc, ability) => {
    const crystal = crystals[ability.crystal.split(' ')[0] as Crystal] as Crystal;
    const type = abilityTypes[ability.type as AbilityType] as AbilityType;

    return {
      ...acc,
      [ability.name]: {
        ...ability,
        crystal,
        recipeTypes: rawData.recipeTypes
          .filter((recipeType) => recipeType[crystal] === ability.name)
          .map(({ type: recipeType }) => recipeType),
        type,
      } as Ability,
    };
  }, {});

export interface Recipe {
  availableFor: { [key in Character]: boolean };
  ingredients: string[];
  result: string;
  type: string;
  '%': number;
}

// const emptyRecipes = rawData.commands
//   .reduce((acc, { name }) => ({ ...acc, [command]: [] }), {} as { [key: string]: Recipe[] });

export const recipes = rawData.recipes
  .reduce<Record<string, Recipe[] | undefined>>((acc, recipe) => ({
    ...acc,
    [recipe.result]: [
      ...(acc[recipe.result] ?? []),
      {
        ...recipe,
        availableFor: {
          [characters.Terra]: recipe.availableFor.includes('T'),
          [characters.Ventus]: recipe.availableFor.includes('V'),
          [characters.Aqua]: recipe.availableFor.includes('A'),
        },
      } as Recipe,
    ],
  }), {});
  // }), emptyRecipes);

interface BaseCommand {
  // eslint-disable-next-line no-unused-vars
  availableFor: { [key in Character]: boolean };
  description: string;
  max: number;
  name: string;
  type: CommandType;
}

export type BattleCommand = BaseCommand & {
  class: string;
  damageType: string;
  slots: number;
};

export type ActionCommand = BaseCommand;

export type ShotlockCommand = BaseCommand & {
  damageType: string;
  shots: number;
};

export type Command = ActionCommand | BattleCommand | ShotlockCommand;

export const commands = rawData.commands
  .reduce<Record<string, Command>>((acc, command) => ({
    ...acc,
    [command.name]: {
      ...command,
      availableFor: {
        [characters.Terra]: command.availableFor.includes('T'),
        [characters.Ventus]: command.availableFor.includes('V'),
        [characters.Aqua]: command.availableFor.includes('A'),
      },
    } as Command,
  }), {});

// enable to generate a new translation file with all the names from the data
// console.log([...Object.keys(abilities), ...Object.keys(commands), ...Object.keys(crystals)].join('\n'));
