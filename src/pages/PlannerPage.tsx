import styled from 'styled-components';
import { t, Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import { useAppSelector } from '../hooks';
import { selectQueuedMelds, selectMeldableCommands } from '../reducer';
import useUpdate from '../useUpdate';

import MeldableCommands from '../components/MeldableCommands';
import Inventory from '../components/Inventory';
import QueuedMelds from '../components/QueuedMelds';

import MenuOption from '../layout/MenuOption';

const INVENTORY = t`Inventory`;
const MELDABLE_COMMANDS = t`Meldable`;
const QUEUED_RECIPES = t`Queued`;
const PAGES = [
  INVENTORY,
  MELDABLE_COMMANDS,
  QUEUED_RECIPES,
] as const;

type PageName = typeof PAGES[number];

const Container = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  height: 100%;
  /* overflow: hidden; */
`;

interface PageContainerProps {
  readonly currentDisplay: PageName;
}

const PageContainer = styled.div<PageContainerProps>`
  display: flex;
  align-items: flex-start;
  height: 100%;
  width: 100%;

  > div {
    flex: 1;
    max-height: 100%;
  }

  @media (max-width:${WIDTH_BREAKPOINT}) {
    height: calc(100% - 35.6px);

    > :not(div:nth-child(${(props) => PAGES.indexOf(props.currentDisplay) + 1})) {
      display: none;
    }
  }
`;

const MobileNavbar = styled.div`
  display: flex;
  align-items: stretch;
  height: 35.6px;

  > div {
    flex: 1;
    margin: 3px
  }

  @media (min-width:${WIDTH_BREAKPOINT}) {
    display: none;
  }
`;

const PlannerPage = () => {
  const [display, updateDisplay] = useUpdate(INVENTORY);

  const queuedMelds = useAppSelector(selectQueuedMelds);
  const meldableCommands = useAppSelector(selectMeldableCommands);

  const counts = {
    [INVENTORY]: 0,
    [MELDABLE_COMMANDS]: meldableCommands.length,
    [QUEUED_RECIPES]: queuedMelds.length,
  };

  return (
    <Container>
      <MobileNavbar>
        {PAGES.map((page) => (
          <MenuOption key={page} active={display === page} onClick={updateDisplay(page)}>
            <Trans id={page} /> {counts[page] > 0 && `(${counts[page]})`}
          </MenuOption>
        ))}
      </MobileNavbar>

      <PageContainer currentDisplay={display}>
        <Inventory />
        <MeldableCommands />
        <QueuedMelds />
      </PageContainer>
    </Container>
  );
};

export default PlannerPage;
