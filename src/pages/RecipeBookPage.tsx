import { useCallback, useState, useMemo } from 'react';
import styled from 'styled-components';
import { t, Trans } from '@lingui/macro';

import type { Command, Recipe } from '../database';
import { commands, recipes } from '../database';
import { useAppSelector, useAppDispatch } from '../hooks';
import {
  selectRecipeBook,
  toggleRecipeBook,
  selectDiscoveredCommands,
  queueShoppingRecipe,
} from '../reducer';

import CommandList from '../components/CommandList';
import CommandRecipes from '../layout/CommandRecipes';
import { i18n } from '@lingui/core';

const Container = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`;

const Sidebar = styled.div`
  height: 100%;
  min-width: 180px;
  overflow-y: scroll;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const CommandsContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-content: flex-start;
  flex-grow: 1;
  height: 100%;
  overflow-y: auto;

  > * {
    margin: 5px;
  }
`;

const RecipeBookPage = () => {
  const dispatch = useAppDispatch();
  const recipeBook = useAppSelector(selectRecipeBook);
  const discovered = useAppSelector(selectDiscoveredCommands);

  const [search, setSearch] = useState('');
  const updateSearch = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => setSearch(value),
    [],
  );
  const [hideDiscovered, setHideDiscovered] = useState(false);
  const updateHideDiscovered = useCallback(
    ({ target: { checked } }: React.ChangeEvent<HTMLInputElement>) => setHideDiscovered(checked),
    [],
  );

  const filter = useCallback(
    (command: Command) => (!search || i18n._(command.name).toLowerCase()
      .includes(search.toLowerCase())) && (!hideDiscovered || !discovered[command.name]),
    [discovered, hideDiscovered, search],
  );

  const addToShoppingList = useCallback(
    (recipe: Recipe) => () => dispatch(queueShoppingRecipe(recipe)),
    [dispatch],
  );

  const commandsInBook = useMemo(
    () => Object.entries(recipeBook)
      .reduce<Command[]>((acc, [name, include]) => [
      ...acc,
      ...(include ? [commands[name]] : []),
    ], []),
    [recipeBook],
  );

  const toggleCommand = useCallback(
    (name: string) => () => dispatch(toggleRecipeBook(name)),
    [dispatch],
  );

  const toggleCommandByIndex = useCallback(
    (index: number) => () => dispatch(toggleRecipeBook(commandsInBook[index].name)),
    [dispatch, commandsInBook],
  );

  return (
    <Container>
      <Sidebar>
        <input
          type="text"
          placeholder={t`Search`}
          onChange={updateSearch}
          value={search}
        />
        <label>
          <input type="checkbox" checked={hideDiscovered} onChange={updateHideDiscovered} />
          {' '}
          <Trans>Hide Discovered Commands</Trans>
        </label>
        <CommandList
          filter={filter}
          onClick={toggleCommand}
        />
      </Sidebar>

      <CommandsContainer>
        {commandsInBook.map((command, index) => (
          <CommandRecipes
            key={command.name}
            command={command}
            index={index}
            onCloseClick={toggleCommandByIndex}
            onRecipeClick={addToShoppingList}
            recipes={recipes[command.name] ?? []}
          />
        ))}
      </CommandsContainer>
    </Container>
  );
};

export default RecipeBookPage;
