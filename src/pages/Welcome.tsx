/* eslint-disable max-len */
import styled from 'styled-components';
import { i18n } from '@lingui/core';
import { Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import { useAppSelector } from '../hooks';
import { selectActiveCharacter } from '../reducer';

import MenuOption from '../layout/MenuOption';

const Container = styled.div`
  display: flex;
  flex-flow: column;
  padding: 10px;
  height: 100%;
  width: 100%;
  overflow-y: auto;
  align-items: center;

  > * {
    max-width: 700px;
  }

  h4 {
    margin: 10px;
  }

  p {
    padding: 0 5px;
    margin-top: 0;
    text-align: justify;
  }

  sup {
    font-size: 0.6em;
  }

  @media (min-width: ${WIDTH_BREAKPOINT}) {
    margin: auto;

    span {
      display: none;
    }
  }
`;

const ExplainerContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: ${WIDTH_BREAKPOINT}) {
    > div {
      display: flex;
      flex-direction: row;
      padding-bottom: 10px;

      ${MenuOption} {
        flex: 1;
        max-width: 300px;
        max-height: 50px;
      }

      p {
        width: 75%;
      }
    }
  }
`;

const Spacer = styled.div`
  height: 100px;
`;

const Welcome = () => {
  const character = useAppSelector(selectActiveCharacter);

  return (
    <Container>
      <h2><Trans>Welcome!</Trans></h2>
      <p>
        <Trans>This is bbsmeldguide.lol by Texonidas (lol). It's an interactive tool for Kingdom Hearts: Birth By Sleep that allows you to track your abilities and discovered commands, and to plan your next round of melding.</Trans>
      </p>
      <h4><Trans>how2use</Trans></h4>
      <ExplainerContainer>
        <div>
          <MenuOption active><Trans>Characters</Trans></MenuOption>
          <p>
            <Trans><span>Click on the portrait of {i18n._(character)} on the bottom left to open the sidebar.</span> We're all internet users, so we understand UI, but juuust in case it isn't clear, click on a character portrait to switch to them.</Trans>
          </p>
        </div>
        <div>
          <MenuOption active><Trans>Abilities</Trans></MenuOption>
          <p>
            <Trans>You can view which recipe types abilities are present on, and track your current ability levels here.<span> Just click on the ability name to toggle it between the counters and the types.</span> Click on empty slots to increase your ability level, click on the full slots to decrease it. That's about it tbh, you should only need to do this once per character, then the tracker will take care of the rest.</Trans>
          </p>
        </div>
        <div>
          <MenuOption active><Trans>Discovered</Trans></MenuOption>
          <p>
            <Trans>Click commands to toggle whether you have discovered them or not. Again, that's about it, thrice and diced.</Trans>
          </p>
        </div>
        <div>
          <MenuOption active><Trans>Planner</Trans></MenuOption>
          <p>
            <Trans>
              The core of this app.<span> We all know how tabs work right? Click to switch between them.</span> Input the commands you have on hand (I put all of them in, but you can leave un-maxed commands out if you want), and the possible meld recipes will be listed out<span> under the meldable tab</span>. Click on the recipe for a command to queue that command for melding<span> under the queued tab</span>. Then you can choose which ability you want to unlock from the dropdown on the queued meld. You can filter the queued melds to only undiscovered commands, and you can also show only recipes that allow for a specific ability unlock.
              <br />
              Once you've executed all your planned melds (single meld coming soon<sup>tm</sup>) in-game, hit meld<span>under the queued tab</span>, and all of your discovered commands, ability counts, and inventory will auto-update based on what was queued.
            </Trans>
          </p>
        </div>
        <div>
          <MenuOption active><Trans>Recipe Book</Trans></MenuOption>
          <p>
            <Trans>A simple lookup tool for recipes. Click a command to see its recipes, click a recipe to see the recipes for its ingredients. Ingreption.</Trans>
          </p>
        </div>
        <div>
          <MenuOption active><Trans>About</Trans></MenuOption>
          <p>
            <Trans>I wrote this because I was sick of going backwards and forth across tables on different websites to track my melding progress. I wanted to make it easy to plan my next round of melding instead of spending half my time flicking between websites instead of actually playing the game. Hopefully it will be as useful to you as it has been to me. If you run into any bugs, PLEASE CONTACT ME SO I CAN FIX IT. I am easily contactable via <a href="https://reddit.com/u/_texonidas_">reddit</a>. I would also love to translate this into as many languages as possible, so if you feel like helping out with translation to your native tongue, just drop me a line.</Trans>
          </p>
        </div>
        <Spacer />
      </ExplainerContainer>
    </Container>
  );
};

export default Welcome;
