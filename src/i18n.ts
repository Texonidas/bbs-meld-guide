
import { i18n } from '@lingui/core';
import type { Messages } from '@lingui/core';
import { defineMessage } from '@lingui/macro';
import { de, en } from 'make-plural/plurals';
import { messages as enMessages } from './locales/en/messages';

export const locales = {
  de: 'Deutsch',
  en: 'English',
  zu: 'Pseudo',
};

export const defaultLocale = 'en';

const localeData = {
  de: { plurals: de },
  en: { plurals: en },
};

i18n.loadLocaleData(localeData);

i18n.load('en', enMessages);
i18n.activate('en');

const supportedRegions = Object.keys(localeData);

export async function dynamicActivate(locale: string) {
  const [region] = locale.split('-');

  const fetchRegion = supportedRegions.includes(region) ? region : 'en';

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const { messages } = await import(`./locales/${fetchRegion}/messages.ts`);

  i18n.load(region, messages as Messages);
  i18n.activate(region);
}

defineMessage({ message: 'Treasure Magnet' });
defineMessage({ message: 'HP Prize Plus' });
defineMessage({ message: 'Link Prize Plus' });
defineMessage({ message: 'Lucky Strike' });
defineMessage({ message: 'HP Boost' });
defineMessage({ message: 'Fire Boost' });
defineMessage({ message: 'Blizzard Boost' });
defineMessage({ message: 'Thunder Boost' });
defineMessage({ message: 'Cure Boost' });
defineMessage({ message: 'Item Boost' });
defineMessage({ message: 'Attack Haste' });
defineMessage({ message: 'Magic Haste' });
defineMessage({ message: 'Combo F Boost' });
defineMessage({ message: 'Finish Boost' });
defineMessage({ message: 'Fire Screen' });
defineMessage({ message: 'Blizzard Screen' });
defineMessage({ message: 'Thunder Screen' });
defineMessage({ message: 'Dark Screen' });
defineMessage({ message: 'Reload Boost' });
defineMessage({ message: 'Defender' });
defineMessage({ message: 'Zero EXP' });
defineMessage({ message: 'Combo Plus' });
defineMessage({ message: 'Air Combo Plus' });
defineMessage({ message: 'EXP Chance' });
defineMessage({ message: 'EXP Walker' });
defineMessage({ message: 'Damage Syphon' });
defineMessage({ message: 'Second Chance' });
defineMessage({ message: 'Once More' });
defineMessage({ message: 'Scan' });
defineMessage({ message: 'Leaf Bracer' });
defineMessage({ message: 'Quick Blitz' });
defineMessage({ message: 'Blitz' });
defineMessage({ message: 'Meteor Crash' });
defineMessage({ message: 'Magic Hour' });
defineMessage({ message: 'Sliding Dash' });
defineMessage({ message: 'Fire Dash' });
defineMessage({ message: 'Dark Haze' });
defineMessage({ message: 'Sonic Blade' });
defineMessage({ message: 'Chaos Blade' });
defineMessage({ message: 'Zantetsuken' });
defineMessage({ message: 'Strike Raid' });
defineMessage({ message: 'Freeze Raid' });
defineMessage({ message: 'Treasure Raid' });
defineMessage({ message: 'Spark Raid' });
defineMessage({ message: 'Wind Raid' });
defineMessage({ message: 'Fire Surge' });
defineMessage({ message: 'Barrier Surge' });
defineMessage({ message: 'Thunder Surge' });
defineMessage({ message: 'Aerial Slam' });
defineMessage({ message: 'Ars Solum' });
defineMessage({ message: 'Ars Arcanum' });
defineMessage({ message: 'Time Splicer' });
defineMessage({ message: 'Poison Edge' });
defineMessage({ message: 'Wishing Edge' });
defineMessage({ message: 'Blizzard Edge' });
defineMessage({ message: 'Stun Edge' });
defineMessage({ message: 'Slot Edge' });
defineMessage({ message: 'Fire Strike' });
defineMessage({ message: 'Confusion Strike' });
defineMessage({ message: 'Binding Strike' });
defineMessage({ message: 'Tornado Strike' });
defineMessage({ message: 'Brutal Blast' });
defineMessage({ message: 'Magnet Spiral' });
defineMessage({ message: 'Salvation' });
defineMessage({ message: 'Windcutter' });
defineMessage({ message: 'Limit Storm' });
defineMessage({ message: 'Collision Magnet' });
defineMessage({ message: 'Geo Impact' });
defineMessage({ message: 'Sacrifice' });
defineMessage({ message: 'Break Time' });
defineMessage({ message: 'Fire' });
defineMessage({ message: 'Fira' });
defineMessage({ message: 'Firaga' });
defineMessage({ message: 'Dark Firaga' });
defineMessage({ message: 'Fission Firaga' });
defineMessage({ message: 'Crawling Fire' });
defineMessage({ message: 'Triple Firaga' });
defineMessage({ message: 'Blizzard' });
defineMessage({ message: 'Blizzara' });
defineMessage({ message: 'Blizzaga' });
defineMessage({ message: 'Triple Blizzaga' });
defineMessage({ message: 'Thunder' });
defineMessage({ message: 'Thundara' });
defineMessage({ message: 'Thundaga' });
defineMessage({ message: 'Thundaga Shot' });
defineMessage({ message: 'Cure' });
defineMessage({ message: 'Cura' });
defineMessage({ message: 'Curaga' });
defineMessage({ message: 'Esuna' });
defineMessage({ message: 'Mine Shield' });
defineMessage({ message: 'Mine Square' });
defineMessage({ message: 'Seeker Mine' });
defineMessage({ message: 'Zero Gravity' });
defineMessage({ message: 'Zero Gravira' });
defineMessage({ message: 'Zero Graviga' });
defineMessage({ message: 'Magnet' });
defineMessage({ message: 'Magnera' });
defineMessage({ message: 'Magnega' });
defineMessage({ message: 'Munny Magnet' });
defineMessage({ message: 'Energy Magnet' });
defineMessage({ message: 'D-Link Magnet' });
defineMessage({ message: 'Aero' });
defineMessage({ message: 'Aerora' });
defineMessage({ message: 'Aeroga' });
defineMessage({ message: 'Faith' });
defineMessage({ message: 'Warp' });
defineMessage({ message: 'Deep Freeze' });
defineMessage({ message: 'Glacier' });
defineMessage({ message: 'Ice Barrage' });
defineMessage({ message: 'Firaga Burst' });
defineMessage({ message: 'Raging Storm' });
defineMessage({ message: 'Tornado' });
defineMessage({ message: 'Mega Flare' });
defineMessage({ message: 'Quake' });
defineMessage({ message: 'Meteor' });
defineMessage({ message: 'Transcendence' });
defineMessage({ message: 'Mini' });
defineMessage({ message: 'Blackout' });
defineMessage({ message: 'Ignite' });
defineMessage({ message: 'Confuse' });
defineMessage({ message: 'Bind' });
defineMessage({ message: 'Poison' });
defineMessage({ message: 'Slow' });
defineMessage({ message: 'Stop' });
defineMessage({ message: 'Stopra' });
defineMessage({ message: 'Stopga' });
defineMessage({ message: 'Sleep' });
defineMessage({ message: 'Jump' });
defineMessage({ message: 'High Jump' });
defineMessage({ message: 'Dodge Roll' });
defineMessage({ message: 'Thunder Roll' });
defineMessage({ message: 'Cartwheel' });
defineMessage({ message: 'Firewheel' });
defineMessage({ message: 'Air Slide' });
defineMessage({ message: 'Ice Slide' });
defineMessage({ message: 'Reversal' });
defineMessage({ message: 'Glide' });
defineMessage({ message: 'Superglide' });
defineMessage({ message: 'Fire Glide' });
defineMessage({ message: 'Sonic Impact' });
defineMessage({ message: 'Slide' });
defineMessage({ message: 'Homing Slide' });
defineMessage({ message: 'Teleport' });
defineMessage({ message: 'Doubleflight' });
defineMessage({ message: 'Block' });
defineMessage({ message: 'Renewal Block' });
defineMessage({ message: 'Focus Block' });
defineMessage({ message: 'Stun Block' });
defineMessage({ message: 'Poison Block' });
defineMessage({ message: 'Barrier' });
defineMessage({ message: 'Renewal Barrier' });
defineMessage({ message: 'Focus Barrier' });
defineMessage({ message: 'Confuse Barrier' });
defineMessage({ message: 'Stop Barrier' });
defineMessage({ message: 'Counter Rush' });
defineMessage({ message: 'Reversal Slash' });
defineMessage({ message: 'Payback Raid' });
defineMessage({ message: 'Counter Hammer' });
defineMessage({ message: 'Payback Fang' });
defineMessage({ message: 'Counter Blast' });
defineMessage({ message: 'Payback Surge' });
defineMessage({ message: 'Aerial Recovery' });
defineMessage({ message: 'Meteor Shower' });
defineMessage({ message: 'Flame Salvo' });
defineMessage({ message: 'Chaos Snake' });
defineMessage({ message: 'Dark Volley' });
defineMessage({ message: 'Bubble Blaster' });
defineMessage({ message: 'Ragnarok' });
defineMessage({ message: 'Thunderstorm' });
defineMessage({ message: 'Bio Barrage' });
defineMessage({ message: 'Pulse Bomb' });
defineMessage({ message: 'Prism Rain' });
defineMessage({ message: 'Photon Charge' });
defineMessage({ message: 'Absolute Zero' });
defineMessage({ message: 'Lightning Ray' });
defineMessage({ message: 'Sonic Shadow' });
defineMessage({ message: 'Ultima Cannon' });
defineMessage({ message: 'Multivortex' });
defineMessage({ message: 'Lightbloom' });
defineMessage({ message: 'Shimmering' });
defineMessage({ message: 'Fleeting' });
defineMessage({ message: 'Pulsing' });
defineMessage({ message: 'Wellspring' });
defineMessage({ message: 'Soothing' });
defineMessage({ message: 'Hungry' });
defineMessage({ message: 'Abounding' });
