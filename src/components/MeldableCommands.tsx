import { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import type { Recipe } from '../database';
import { abilities, commands, recipes } from '../database';
import { useAppSelector, useAppDispatch } from '../hooks';
import {
  queueMeld,
  selectUsedCommands,
  selectInventory,
  selectActiveCharacter,
  selectMeldableCommands,
  isMeldable,
  selectDiscoveredCommands,
  selectUnmaxedAbilities,
} from '../reducer';

import CommandRecipes from '../layout/CommandRecipes';

import { CHARACTER_ICON_SIZE } from './Sidebar';

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-evenly;
  align-content: flex-start;
  overflow: auto;

  h3,
  label {
    width: 100%;
  }

  > div {
    margin: 5px;
  }

  @media screen and (max-width: ${WIDTH_BREAKPOINT}) {
    padding-bottom: ${CHARACTER_ICON_SIZE};
  }
`;

const AvailableCommands = () => {
  const character = useAppSelector(selectActiveCharacter);
  const inventory = useAppSelector(selectInventory);
  const used = useAppSelector(selectUsedCommands);
  const discovered = useAppSelector(selectDiscoveredCommands);
  const meldableCommands = useAppSelector(selectMeldableCommands);
  const unmaxedAbilities = useAppSelector(selectUnmaxedAbilities);

  const dispatch = useAppDispatch();

  const [hideDiscovered, setHideDiscovered] = useState(false);
  const updateHideDiscovered = useCallback(
    ({ target: { checked } }: React.ChangeEvent<HTMLInputElement>) => setHideDiscovered(checked),
    [],
  );

  const [withAbility, setWithAbility] = useState('');
  const updateWithAbility = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLSelectElement>) => setWithAbility(value),
    [],
  );
  const clearWithAbility = useCallback(() => setWithAbility(''), []);

  const selectRecipe = useCallback((recipe: Recipe) => () => {
    dispatch(queueMeld(recipe));
  }, [dispatch]);

  const currentlyMeldable = useMemo(
    () => isMeldable(character, inventory, used),
    [character, inventory, used],
  );

  const filteredCommands = useMemo(
    () => meldableCommands.filter((name) => !hideDiscovered || !discovered[name]),
    [discovered, hideDiscovered, meldableCommands],
  );

  const filterRecipes = useCallback(
    (recipe: Recipe) => currentlyMeldable(recipe) &&
      (!withAbility || abilities[withAbility].recipeTypes.includes(recipe.type)),
    [currentlyMeldable, withAbility],
  );

  return (
    <Container>
      <h3><Trans>Meldable Commands</Trans></h3>

      {meldableCommands.length ? (
        <>
          <label>
            <input type="checkbox" checked={hideDiscovered} onChange={updateHideDiscovered} />
            {' '}
            <Trans>Hide Discovered Commands</Trans>
          </label>
          <label>
            <Trans>Recipes with ability:</Trans>
            {' '}
            <select onChange={updateWithAbility} value={withAbility}>
              <option value="">--</option>
              {unmaxedAbilities.map((ability) => (
                <option key={ability} value={ability}><Trans id={ability} /></option>
              ))}
            </select>

            {withAbility && <span onClick={clearWithAbility}>&times;</span>}
          </label>
        </>
      ) : (
        <p><Trans>No meldable commands</Trans></p>
      )}

      {filteredCommands.map((name, index) => (
        <CommandRecipes
          key={name}
          command={commands[name]}
          index={index}
          onRecipeClick={selectRecipe}
          recipes={(recipes[name] ?? []).filter(filterRecipes)}
        />
      ))}
    </Container>
  );
};

export default AvailableCommands;
