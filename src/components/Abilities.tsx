import { useMemo } from 'react';
import styled from 'styled-components';

import { WIDTH_BREAKPOINT } from '../css.constants';
import { abilities } from '../database';
import type { Ability, AbilityType } from '../database';
import { useAppSelector } from '../hooks';
import { selectAbilityCounts } from '../reducer';

import AbilityWrapper from '../layout/AbilityWrapper';
import FancyAbility from '../layout/FancyAbility';
import { CHARACTER_ICON_SIZE } from './Sidebar';

const Container = styled.div`
  height: 100%;
  overflow-x: hidden;
  padding: 2px;
  margin: auto;
  display: flex;
  flex-flow: column nowrap;
`;

const TypeContainer = styled.div`
  display: flex;


  > div {
    flex: 1;

    &:first-child {
      min-width: 120px;
      max-width: 20%;
    }
  }

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    flex-flow: column nowrap;

    &:last-child {
      padding-bottom: ${CHARACTER_ICON_SIZE};
    }

    > div:first-child {
      width: 90%;
      margin-right: auto;
    }
  }
`;

const AbilityContainer = styled.div`
  max-width: 700px;
  flex: 1;

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    width: 90%;
    margin-left: auto;
  }
`;

const Abilities = () => {
  const abilityCounts = useAppSelector(selectAbilityCounts);
  const abilitiesByType = useMemo(() => Object.values(abilities)
    .reduce<{ [type in AbilityType]: Ability[] }>((acc, ability) => ({
      ...acc,
      [ability.type]: [...acc[ability.type], ability],
    }), { Prizes: [], Stats: [], Support: [] }), []);

  return (
    <Container>
      {(Object.keys(abilitiesByType) as AbilityType[]).map((type) => {
        const typeAbilities = abilitiesByType[type];

        return (
          <TypeContainer key={type}>
            <div>
              <AbilityWrapper isLabel text={type} type={type} />
            </div>

            <AbilityContainer>
              {typeAbilities.map((ability) => (
                <FancyAbility
                  key={ability.name}
                  ability={ability}
                  count={abilityCounts[ability.name] || 0}
                />
              ))}
            </AbilityContainer>
          </TypeContainer>
        );
      })}
    </Container>
  );
};

export default Abilities;
