import styled from 'styled-components';

import { WIDTH_BREAKPOINT } from '../css.constants';
import type { Command } from '../database';
import { commands } from '../database';

import { CHARACTER_ICON_SIZE } from './Sidebar';

import FancyCommand from '../layout/FancyCommand';
import { useAppSelector } from '../hooks';
import { selectActiveCharacter } from '../reducer';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;

  @media screen and (max-width: ${WIDTH_BREAKPOINT}) {
    padding-bottom: ${CHARACTER_ICON_SIZE};
  }
`;

interface CommandListProps {
  filter?: (command: Command) => boolean;
  onClick: (name: string) => () => void;
}

const NO_FILTER = () => true;
const CommandList = ({ filter = NO_FILTER, onClick }: CommandListProps) => {
  const activeCharacter = useAppSelector(selectActiveCharacter);

  return (
    <Container>
      {Object.values(commands)
        .filter((command) => command.availableFor[activeCharacter] && filter(command))
        .map((command) => (
          <FancyCommand
            key={command.name}
            command={command}
            onClick={onClick(command.name)}
          />
        ))}
    </Container>
  );
};

CommandList.defaultProps = {
  filter: NO_FILTER,
};

export default CommandList;
