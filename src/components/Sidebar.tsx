/* eslint-disable no-magic-numbers */
import { useCallback, useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import type { Character } from '../database';
import { characters } from '../database';
import { useAppSelector, useAppDispatch } from '../hooks';
import {
  addFeedbackModalPop,
  chooseCharacter,
  selectActiveCharacter,
  selectFeedbackModalReady,
  updateSessionTime,
} from '../reducer';
import useToggle from '../useToggle';

import FeedbackModal from './FeedbackModal';

import MenuOption from '../layout/MenuOption';

import terraThumb from '../img/terra.webp';
import ventusThumb from '../img/ventus.webp';
import aquaThumb from '../img/aqua.webp';

interface ClosableProps {
  readonly isOpen?: boolean;
}

const Container = styled.div<ClosableProps>`
  padding: 15px;
  min-width: 230px;
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: space-between;
  position: relative;
  overflow: hidden;

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    ${(props) => props.isOpen ? css`
      width: 60%;
    ` : css`
      width: 0;
      min-width: 0;
      margin: 0;
      padding: 0;
    `}
    position: absolute;
    left: 0;
    top: 0;
    background: white;
    z-index: 1000;
  }
`;

const Close = styled.div`
  position: absolute;
  top: 2vh;
  right: 2vh;

  @media (min-width: ${WIDTH_BREAKPOINT}) {
    display: none;
  }
`;

const NavContainer = styled.div<ClosableProps>`
  display: flex;
  flex-direction: column;
  margin: 20px;

  > * {
    margin: 1px;
  }

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    ${(props) => props.isOpen ? '' : 'display: none;'}
  };
`;

const StyledLink = styled(Link)`
  color: white !important;
  text-decoration: none;
`;

const ThumbContainer = styled.div<ClosableProps>`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
`;

interface CharacterThumbProps extends ClosableProps {
  readonly active: boolean;
}

const THUMB_WIDTH = 75;
const THUMB_MARGIN = 5;
const THUMB_POSITION = 15;

export const CHARACTER_ICON_SIZE = `${THUMB_WIDTH + (2 * (THUMB_MARGIN + THUMB_POSITION))}px`;

const INACTIVE_OPACITY = 0.5;
const CharacterThumb = styled.img<CharacterThumbProps>`
  opacity: ${(props) => props.active ? 1 : INACTIVE_OPACITY};
  margin: ${THUMB_MARGIN}px;
  cursor: pointer;
  user-select: none;
  border-radius: 50%;
  box-shadow: 0 0 3px black;
  max-width: 40px;
  flex: 1;

  ${(props) => props.active
    ? css`
      height: ${THUMB_WIDTH}px;
      max-width: ${THUMB_WIDTH}px;

      @media (max-width: ${WIDTH_BREAKPOINT}) {
        ${props.isOpen ? '' : css`
          position: fixed;
          bottom: ${THUMB_POSITION}px;
          left: ${THUMB_POSITION}px;
        `}
      }
    ` : css`
      @media (max-width: ${WIDTH_BREAKPOINT}) {
        ${props.isOpen ? '' : css`
          display: none;
        `}
      }
    `}
`;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 60%;
  width: 40%;
  height: 100%;
  z-index: 100;
  background-color: rgba(0, 0, 0, 0.5);

  @media (min-width: ${WIDTH_BREAKPOINT}) {
    display: none;
  }
`;

const SOURCES = {
  [characters.Terra]: terraThumb,
  [characters.Ventus]: ventusThumb,
  [characters.Aqua]: aquaThumb,
// eslint-disable-next-line no-unused-vars
} as { [key in Character]: string };

const CHARACTERS = [
  characters.Terra,
  characters.Ventus,
  characters.Aqua,
] as Character[];

const TICK = 5000;

const Sidebar = () => {
  const dispatch = useAppDispatch();

  const character = useAppSelector(selectActiveCharacter);
  const feedbackModalReady = useAppSelector(selectFeedbackModalReady);

  const location = useLocation();

  const [isOpen, toggleOpen] = useToggle(false);
  const [blockFeedbackModal, setBlockFeedbackModal] = useState(false);
  const [isFeedbackModalOpen, setIsFeedbackModalOpen] = useState(false);

  const updateCharacter = useCallback(
    (newCharacter: Character) => () => {
      dispatch(chooseCharacter(newCharacter));
      toggleOpen();
    },
    [dispatch, toggleOpen],
  );

  useEffect(() => {
    let lastTimerUpdate = Date.now();

    const intervalId = setInterval(() => {
      const now = Date.now();
      const delta = now - lastTimerUpdate;

      lastTimerUpdate = now;

      if (delta < 2 * TICK) {
        dispatch(updateSessionTime(delta));
      }
    }, TICK);

    return () => clearInterval(intervalId);
  }, [dispatch]);

  useEffect(() => {
    setIsFeedbackModalOpen(!blockFeedbackModal && feedbackModalReady);
  }, [blockFeedbackModal, feedbackModalReady]);

  const closeModal = useCallback((block = false) => {
    dispatch(addFeedbackModalPop());
    setBlockFeedbackModal(block);
    setIsFeedbackModalOpen(false);
  }, [dispatch]);

  return (
    <>
      {isFeedbackModalOpen && <FeedbackModal close={closeModal} />}
      <Container isOpen={isOpen}>
        {isOpen && <Close onClick={toggleOpen}>X</Close>}
        <NavContainer isOpen={isOpen}>
          <StyledLink to="/planner" onClick={toggleOpen}>
            <MenuOption active={location.pathname === '/planner'}>
              <Trans>Planner</Trans>
            </MenuOption>
          </StyledLink>
          <StyledLink to="/recipes" onClick={toggleOpen}>
            <MenuOption active={location.pathname === '/recipes'}>
              <Trans>Recipe Book</Trans>
            </MenuOption>
          </StyledLink>
          <StyledLink to="/abilities" onClick={toggleOpen}>
            <MenuOption active={location.pathname === '/abilities'}>
              <Trans>Abilities</Trans>
            </MenuOption>
          </StyledLink>
          <StyledLink to="/discovered" onClick={toggleOpen}>
            <MenuOption active={location.pathname === '/discovered'}>
              <Trans>Discovered</Trans>
            </MenuOption>
          </StyledLink>
          <StyledLink to="/" onClick={toggleOpen}>
            <MenuOption active={location.pathname === '/'}>
              <Trans>About</Trans>
            </MenuOption>
          </StyledLink>
        </NavContainer>

        <ThumbContainer isOpen={isOpen}>
          {CHARACTERS.map((characterName) => (
            <CharacterThumb
              key={characterName}
              active={character === characterName}
              alt={characterName}
              isOpen={isOpen}
              onClick={updateCharacter(characterName)}
              src={SOURCES[characterName]}
            />
          ))}
        </ThumbContainer>

      </Container>
      {isOpen && <Overlay onClick={toggleOpen} />}
    </>
  );
};

export default Sidebar;
