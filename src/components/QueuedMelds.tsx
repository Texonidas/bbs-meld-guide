import { useCallback } from 'react';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import { commands } from '../database';
import { useAppSelector, useAppDispatch } from '../hooks';
import { executeQueuedMelds, removeQueuedMeld, selectQueuedMelds } from '../reducer';

import { CHARACTER_ICON_SIZE } from './Sidebar';

import CommandRecipes from '../layout/CommandRecipes';

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-evenly;
  align-content: flex-start;
  overflow: auto;

  h3 {
    width: 100%;
  }

  #meld-button   {
    width: 100%;
  }

  > div {
    margin: 5px;
  }

  button {
    margin: auto;
  }

  @media screen and (max-width: ${WIDTH_BREAKPOINT}) {
    padding-bottom: ${CHARACTER_ICON_SIZE};
  }
`;

const AvailableCommands = () => {
  const queuedMelds = useAppSelector(selectQueuedMelds);

  const dispatch = useAppDispatch();

  const executeSelectedMelds = useCallback(() => {
    dispatch(executeQueuedMelds());
  }, [dispatch]);

  const unselectMeld = useCallback((index: number) => () => {
    dispatch(removeQueuedMeld(index));
  }, [dispatch]);

  return (
    <Container>
      <h3><Trans>Queued Melds</Trans></h3>

      {queuedMelds.length > 0 ? (
        <div id="meld-button">
          <button onClick={executeSelectedMelds} type="button">
            <Trans>EXECUTE MELDS</Trans>
          </button>
        </div>
      ) : (
        <p><Trans>No queued melds</Trans></p>
      )}

      {queuedMelds.map(({ recipe, crystal }, index) => (
        <CommandRecipes
          // eslint-disable-next-line react/no-array-index-key
          key={`${recipe.ingredients.toString()}-${index}`}
          command={commands[recipe.result]}
          crystal={crystal}
          index={index}
          onCloseClick={unselectMeld}
          recipes={[recipe]}
          queued
        />
      ))}
    </Container>
  );
};

export default AvailableCommands;
