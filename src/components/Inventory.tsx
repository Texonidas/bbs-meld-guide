import { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import { i18n } from '@lingui/core';
import { t, Trans } from '@lingui/macro';

import { WIDTH_BREAKPOINT } from '../css.constants';
import { commands } from '../database';
import { useAppSelector, useAppDispatch } from '../hooks';
import {
  selectInventory,
  incrementInventoryCommand,
  decrementInventoryCommand,
  discoverCommand,
  selectActiveCharacter,
} from '../reducer';

import FancyCommand from '../layout/FancyCommand';

import { CHARACTER_ICON_SIZE } from './Sidebar';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: stretch;
  height: 100%;
  overflow-y: auto;

  @media (max-width: ${WIDTH_BREAKPOINT}) {
    padding-bottom: ${CHARACTER_ICON_SIZE};
  }
`;

const CommandContainer = styled.div`
  display: flex;
`;

interface CountContainerProps {
  readonly count: number;
}

const CountContainer = styled.div<CountContainerProps>`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  color: ${(props) => props.count ? 'black' : '#BBB'};
  user-select: none;

  > * {
    width: 1.8rem;
    cursor: pointer;

    &:last-child {
      color: black;
    }

    input {
      width: 100%;
      cursor: default;
    }
  }

`;

const Inventory = () => {
  const character = useAppSelector(selectActiveCharacter);
  const inventory = useAppSelector(selectInventory);
  const dispatch = useAppDispatch();

  const decrement = useCallback(
    (command: string) => () => dispatch(decrementInventoryCommand(command)),
    [dispatch],
  );
  const increment = useCallback(
    (command: string) => () => {
      dispatch(incrementInventoryCommand(command));
      dispatch(discoverCommand(command));
    },
    [dispatch],
  );

  const [search, setSearch] = useState('');
  const updateSearch = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => setSearch(value),
    [],
  );

  const filtered = useMemo(
    () => Object.values(commands)
      .filter((command) => command.availableFor[character] &&
        (!search || i18n._(command.name).toLowerCase()
          .includes(search.toLowerCase()))),
    [character, search],
  );

  return (
    <Container>
      <h3><Trans>Inventory</Trans></h3>
      <input
        type="text"
        placeholder={t`Search`}
        onChange={updateSearch}
        value={search}
      />
      {filtered.map((command) => (
        <CommandContainer key={command.name}>
          <FancyCommand command={command} />

          <CountContainer count={inventory[command.name] || 0}>
            <div onClick={decrement(command.name)}>&#x25C0;</div>
            <div>{inventory[command.name] || 0}</div>
            <div onClick={increment(command.name)}>&#x25B6;</div>
          </CountContainer>
        </CommandContainer>
      ))}
    </Container>
  );
};

export default Inventory;
