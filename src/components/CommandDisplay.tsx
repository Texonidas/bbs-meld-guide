import React, { useCallback, useState, useMemo } from 'react';
import styled from 'styled-components';
import { Trans, t } from '@lingui/macro';

import { commands, recipes } from '../database';

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
`;

const OwnContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  height: 100%;
  width: 33%;
  font-size: inherit;
`;

const ChildContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 66%;
  overflow-y: auto;
  overflow-x: hidden;
  font-size: 0.9em;
  border: 1px solid #333;
`;

const CommandContainer = styled.div`
  display: flex;
  flex-flow: wrap row;
  padding: 5px;

  > div {
    border: 1px solid #333;
    border-radius: 3px;
  }
`;

interface CommandDisplayProps {
  commandSlug: string;
  depth?: number;
  hideTooltip: () => void;
  // eslint-disable-next-line no-unused-vars
  showTooltip: (type: string) => () => void;
  type?: string;
}

const CommandDisplay = ({
  commandSlug,
  depth = 0,
  hideTooltip,
  showTooltip,
  type = '',
}: CommandDisplayProps) => {
  const command = useMemo(() => commands[commandSlug], [commandSlug]);
  const [showChildren, setShowChildren] = useState(false);
  const toggleChildren = useCallback(() => setShowChildren(!showChildren), [showChildren]);

  const commandRecipes = useMemo(() => (recipes[command.name] ?? []), [command.name]);
  const childrenAvailable = commandRecipes.length > 0 && depth <= 3;

  return (
    <Container>
      <OwnContainer>
        <CommandContainer>
          <div>
            {command.name}
            {type && (
              <div onMouseOver={showTooltip(type)} onMouseLeave={hideTooltip}>
                {' '}{type}
              </div>
            )}
          </div>

          {childrenAvailable && (
            <button onClick={toggleChildren} type="button">
              <Trans>{showChildren ? t`Hide` : t`Show`} Recipes</Trans>
            </button>
          )}
        </CommandContainer>
      </OwnContainer>

      {showChildren && (
        <ChildContainer>
          {commandRecipes.map((recipe) => (
            <React.Fragment key={recipe.ingredients.toString()}>
              <CommandDisplay
                commandSlug={recipe.ingredients[0]}
                depth={depth + 1}
                hideTooltip={hideTooltip}
                showTooltip={showTooltip}
                type={recipe.type}
              />

              <CommandDisplay
                commandSlug={recipe.ingredients[1]}
                depth={depth + 1}
                hideTooltip={hideTooltip}
                showTooltip={showTooltip}
                type={recipe.type}
              />
            </React.Fragment>
          ))}
        </ChildContainer>
      )}
    </Container>
  );
};

CommandDisplay.defaultProps = {
  depth: 0,
  type: '',
};

export default CommandDisplay;
