import styled from 'styled-components';

import type { Crystal } from '../database';
import { crystals, recipeTypes } from '../database';
import { toCapitalCase } from '../utils';

const Container = styled.div`
  position: absolute;
  top: 0;
  right: 0;
`;

interface TypeTooltipProps {
  type: string;
}

const TypeTooltip = ({ type }: TypeTooltipProps) => (
  <Container>
    <table>
      <tbody>
        {(Object.values(crystals) as Crystal[]).map((crystal) => (
          <tr key={crystal}>
            {toCapitalCase(crystal)}: {recipeTypes[type][crystal]}
          </tr>
        ))}
      </tbody>
    </table>
  </Container>
);

export default TypeTooltip;
