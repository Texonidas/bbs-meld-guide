/* eslint-disable max-len */
import { t, Trans } from '@lingui/macro';
import { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';

import { useAppSelector, useAppDispatch } from '../hooks';
import {
    addFeedbackModalPop,
    selectSessionTotalMinutes,
    selectBeforeDisableFeedback
} from '../reducer';
import { loadState } from '../localStorage';
import useToggle from '../useToggle';

const Overlay = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  background: rgb(0, 0, 0, 0.8);
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 2000;
`;

const Modal = styled.div`
  margin-top: 50px;
  width: 400px;
  max-width: 90%;
  z-index: 2001;
  background: white;
  border-radius: 10px;
  padding: 20px 40px;
  display: flex;
  flex-direction: column;
  align-items: stretch;

  h1 {
    font-size: 190px;
  }

  p {
    text-align: justify;
  }

  textarea {
    width: 100%;
  }

  label {
    padding: 3px;
  }

  button {
    width: 50%;
    margin: auto;
  }
`;

interface FeedbackModalProps {
  close: (block?: boolean) => void;
}

const EMOJI_TEXT = 'bazroople';
const EMOJI_DELAY = 2000;

const FeedbackModal = ({ close }: FeedbackModalProps) => {
  const dispatch = useAppDispatch();
  const sessionTime = useAppSelector(selectSessionTotalMinutes);
  const beforeDisableFeedback = useAppSelector(selectBeforeDisableFeedback);

  const [dontShowAgain, toggleDontShowAgain] = useToggle(false);

  const [text, setText] = useState('');
  const updateText = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLTextAreaElement>) => setText(value),
    [],
  );

  useEffect(() => {
    dispatch(addFeedbackModalPop);
  }, [dispatch]);

  const [sent, setSent] = useState(false);

  const startClose = useCallback(() => {
    setText(EMOJI_TEXT);
    setTimeout(() => close(dontShowAgain), EMOJI_DELAY);
  }, [close, dontShowAgain]);

  const sendFeedback = useCallback(() => {
    if (text) {
      fetch('https://eohyrphqsgufu2w.m.pipedream.net', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          state: JSON.stringify(loadState(), null, 2),
          text,
        }),
      });

      setSent(true);
    }
    else {
      startClose();
    }
  }, [startClose, text]);

  return (
    <Overlay>
      <Modal>
        {text === EMOJI_TEXT ? (
          <h1>{sent ? '😄' : '😞'}</h1>
        ) : (
          <>
            <h4><Trans>HOLY FUCK</Trans></h4>

            {sent ? (
              <>
                <p>
                  <Trans>
                    Wow, thank you so much for giving feedback! Hopefully it was positive lol.
                  </Trans>
                </p>

                {dontShowAgain ? (
                  <>
                    <p>
                      <Trans>
                        The popup is no more! Just a heads up though, it will come back once you reload the page.
                      </Trans>
                    </p>

                    {beforeDisableFeedback && (
                      <p>
                        <Trans>
                          This is a special message that you will only see if you used this tool before I added this disable flag. I apologise for the frequency that the feedback modal was popping up. I'm very keen to improve this however I can, but I don't want to be annoying about it lol. I hope you understand :) Thank you for using my tool, and thank you to all of you have sent positive feedback, it's honestly really nice to know that this is helping people. Regards, Texonidas (lol)
                        </Trans>
                      </p>
                    )}
                  </>
                ) : (
                  <p>
                    <Trans>
                      I'll pop this up in about 2 hours, so let me know when it comes up again if that's annoying.
                    </Trans>
                  </p>
                )}
                <button type="button" onClick={startClose}><Trans>BACK TO THE GRIND BABY</Trans></button>
              </>
            ) : (
              <>
                <p><Trans>You've been using bbsmeldguide.lol for {sessionTime.toFixed(0)} minutes (since I added a session timer anyway lol). Seeing as you're now basically an expert on this app, I'd love it if you could give me some feedback or suggestions (make sure to include your email if you'd like a reply). Nice comments are also appreciated :)</Trans></p>

                <textarea
                  rows={4}
                  onChange={updateText}
                  placeholder={t`Wow, such good app, many wow`}
                  value={text}
                />

                <label>
                  <input
                    type="checkbox"
                    id="dont-show-again"
                    checked={dontShowAgain}
                    onChange={toggleDontShowAgain}
                  /> <Trans>Don't ask for feedback again this session</Trans>
                </label>

                <div>
                  <button type="button" onClick={sendFeedback}><Trans>SEND IT</Trans></button>
                  <button type="button" onClick={startClose}><Trans>I'd rather not, soz</Trans></button>
                </div>
              </>
            )}
          </>
        )}
      </Modal>
    </Overlay>
  );
};

export default FeedbackModal;
