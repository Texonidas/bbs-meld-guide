import { useMemo } from 'react';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';

import { useAppSelector } from '../hooks';
import { commands } from '../database';
import type { Command, Character, CommandType } from '../database';
import { selectDiscoveredCommands, selectActiveCharacter } from '../reducer';

import CommandThumbnail from '../layout/CommandThumbnail';
import MenuOption from '../layout/MenuOption';


const Container = styled.div`
  display: flex;
  flex-flow: column nowrap;
  max-height: 100%;
  overflow: auto;
`;

const TypeContainer = styled.div`
  display: flex;
`;

const LabelContainer = styled.div`
  width: 29%;
  padding: 2px 0;
  margin-top: 3px;
`;

const CommandContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  max-width: 71%;
`;

const getCommandsByType = (character: Character) => Object.values(commands)
  .filter((command) => command.availableFor[character])
  .reduce<{ [type in CommandType]: Command[] }>((acc, command) => ({
    ...acc,
    [command.type]: [...acc[command.type], command],
  }), {
    Attack: [],
    Magic: [],
    Movement: [],
    Defence: [],
    Reprisal: [],
    Shotlock: [],
  });

const DiscoveredCommands = () => {
  const discoveredCommands = useAppSelector(selectDiscoveredCommands);
  const character = useAppSelector(selectActiveCharacter);

  const commandsByType = useMemo(() => getCommandsByType(character), [character]);

  return (
    <Container>
      <h3><Trans>Discovered Commands</Trans></h3>

      {Object.entries(commandsByType).map(([type, typeCommands]) => (
        <TypeContainer key={type}>
          <LabelContainer>
            <MenuOption>
              <Trans id={type} />
            </MenuOption>
          </LabelContainer>

          <CommandContainer>
            {typeCommands.map((command) => (
              <CommandThumbnail
                key={command.name}
                active={Boolean(discoveredCommands[command.name])}
                command={command}
              />
            ))}
          </CommandContainer>
        </TypeContainer>
      ))}
    </Container>
  );
};

export default DiscoveredCommands;
