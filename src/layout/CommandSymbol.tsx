import { i18n } from '@lingui/core';

import type { CommandType } from '../database';
import { commandTypes } from '../database';

import attackIcon from '../img/attack.webp';
import magicIcon from '../img/magic.webp';
import movementIcon from '../img/movement.webp';
import defenceIcon from '../img/defence.webp';
import reprisalIcon from '../img/reprisal.webp';
import shotlockIcon from '../img/shotlock.webp';

const COMMAND_ICONS = {
  [commandTypes.Attack]: attackIcon,
  [commandTypes.Magic]: magicIcon,
  [commandTypes.Movement]: movementIcon,
  [commandTypes.Defence]: defenceIcon,
  [commandTypes.Reprisal]: reprisalIcon,
  [commandTypes.Shotlock]: shotlockIcon,
};

interface CommandSymbolProps {
  className?: string;
  type: CommandType;
}

const CommandSymbol = ({ className, type }: CommandSymbolProps) => (
  <img className={className} src={COMMAND_ICONS[type]} alt={i18n._(type)} />
);

CommandSymbol.defaultProps = {
  className: '',
};

export default CommandSymbol;
