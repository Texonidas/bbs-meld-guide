import { useCallback, useMemo } from 'react';
import styled from 'styled-components';

import AbilityWrapper from './AbilityWrapper';

import { WIDTH_BREAKPOINT } from '../css.constants';
import type { Ability } from '../database';
import { useAppDispatch } from '../hooks';
import { decrementAbility, incrementAbility } from '../reducer';
import useToggle from '../useToggle';

import abilityLocked from '../img/ability-locked.webp';
import abilityUnlocked from '../img/ability-unlocked.webp';

interface ToggleableProps {
  displaying: boolean;
}

const UnlockedContainer = styled.div<ToggleableProps>`
  max-height: 100%;
  display: flex;

  @media screen and (max-width: ${WIDTH_BREAKPOINT}) {
    ${(props) => props.displaying ? '' : 'display: none;'}
  }
`;

interface UnlockableProps {
  isUnlocked: boolean;
}

const UnlockWrapper = styled.div<UnlockableProps>`
  position: relative;
  height: calc(28px - ${(props) => props.isUnlocked ? '0px' : '2px'});
  width: 28px;
  cursor: pointer;
`;


const Image = styled.img<UnlockableProps>`
  position: absolute;
  height: calc(28px - ${(props) => props.isUnlocked ? '0px' : '2px'});
  width: calc(28px - ${(props) => props.isUnlocked ? '0px' : '2px'});
  cursor: pointer;
  z-index: 10;
`;

const TypeContainer = styled.div<ToggleableProps>`
  padding: 0 5px;

  @media screen and (max-width: ${WIDTH_BREAKPOINT}) {
    ${(props) => props.displaying ? '' : 'display: none;'}
    padding-left: 15px;
  }
`;

interface FancyAbilityProps {
  ability: Ability;
  count: number;
}

const FancyAbility = ({ ability, count }: FancyAbilityProps) => {
  const [showRecipeTypes, toggleShowRecipeTypes] = useToggle(false);

  const dispatch = useAppDispatch();

  const decrement = useCallback(
    () => dispatch(decrementAbility(ability.name)),
    [ability.name, dispatch],
  );
  const increment = useCallback(
    () => dispatch(incrementAbility(ability.name)),
    [ability.name, dispatch],
  );

  const unlockedCount = useMemo(
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    () => [...Array(ability.max)].map((_, i) => i < count),
    [ability.max, count],
  );

  return (
    <AbilityWrapper onClick={toggleShowRecipeTypes} text={ability.name} type={ability.type}>
      <>
        <UnlockedContainer displaying={!showRecipeTypes}>
          {unlockedCount.map((isUnlocked, i) => (
            <UnlockWrapper
              // eslint-disable-next-line react/no-array-index-key
              key={`${i}-${isUnlocked.toString()}`}
              isUnlocked={isUnlocked}
              onClick={isUnlocked ? decrement : increment}
            >
              <Image isUnlocked={isUnlocked} src={isUnlocked ? abilityUnlocked : abilityLocked} />
            </UnlockWrapper>
          ))}
        </UnlockedContainer>

        <TypeContainer displaying={showRecipeTypes} onClick={toggleShowRecipeTypes}>
          [{ability.recipeTypes.join(', ')}]
        </TypeContainer>
      </>
    </AbilityWrapper>
  );
};

export default FancyAbility;
