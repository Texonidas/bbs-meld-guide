import { i18n } from '@lingui/core';
import styled from 'styled-components';

import type { AbilityType } from '../database';
import { abilityTypes } from '../database';

import prizes from '../img/prizes.webp';
import stats from '../img/stats.webp';
import support from '../img/support.webp';

const TYPE_MARKERS = {
  [abilityTypes.Prizes]: prizes,
  [abilityTypes.Stats]: stats,
  [abilityTypes.Support]: support,
};

interface AbilityMarkerProps {
  className?: string;
  type: AbilityType;
}

const Image = styled.img`
  height: 25px;
  width: 25px;
  z-index: 10;
`;

const AbilityMarker = ({ className, type }: AbilityMarkerProps) => (
  // eslint-disable-next-line react/forbid-component-props
  <Image className={className} src={TYPE_MARKERS[type]} alt={i18n._(type)} />
);

AbilityMarker.defaultProps = {
  className: '',
};

export default AbilityMarker;
