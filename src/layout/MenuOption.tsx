import styled, { css } from 'styled-components';

interface MenuOptionProps {
  readonly active?: boolean;
}

const MenuOption = styled.div<MenuOptionProps>`
  ${(props) => props.active
    ? css`
      background: linear-gradient(black 0%, black 30%, red 70%, red 100%);
      border: 2px solid red;
      padding: 2px 9px;
    ` : css`
      background-color: black;
      border-top: 1px solid white;
      border-left: 1px solid white;
      padding: 3px 10px;
    `}
  color: white;
  text-align: left;

  mask:
    linear-gradient(45deg, transparent 0 0px , #fff 0) bottom left,
    linear-gradient(-45deg, transparent 0 6px, #fff 0) bottom right,
    linear-gradient(135deg, transparent 0 6px, #fff 0) top left,
    linear-gradient(-135deg, transparent 0 0px, #fff 0) top right;
  mask-size: 52% 52%;
  mask-repeat: no-repeat;
`;

export default MenuOption;
