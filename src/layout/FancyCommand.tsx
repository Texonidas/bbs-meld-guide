/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable no-magic-numbers */
import styled, { css } from 'styled-components';
import { darken, lighten } from 'polished';
import { Trans } from '@lingui/macro';

import CommandSymbol from './CommandSymbol';

import type { Command } from '../database';
import { commandTypes } from '../database';

const sharedSize = css`
  height: 25.6px;
  margin: 2px;
  width: 160px;
`;

const COMMAND_COLOURS = {
  [commandTypes.Attack]: '#001897',
  [commandTypes.Magic]: '#001897',
  [commandTypes.Movement]: '#B90000',
  [commandTypes.Defence]: '#B90000',
  [commandTypes.Reprisal]: '#B90000',
  [commandTypes.Shotlock]: '#00B809',
};

const Container = styled.div`
  ${sharedSize}
  position: relative;
`;

const Background = styled.div`
  ${sharedSize}
  position: absolute;
  top: 0;
  left: 0;
  background-color: ${(props) => props.color};
  border: 2px solid ${(props) => props.color ?? '#333'};
  border-top:  2px solid ${(props) => lighten(0.2, props.color ?? '#333')};
  border-bottom: 2px solid ${(props) => darken(0.2, props.color ?? '#333')};
  border-radius: 0 15px 15px 0;
  z-index: -2;

  /* from: https://stackoverflow.com/questions/7324722/cut-corners-using-css */
  mask:
    linear-gradient(45deg, transparent 0 0px , #fff 0) bottom left,
    linear-gradient(-45deg, transparent 0 0px, #fff 0) bottom right,
    linear-gradient(135deg, transparent 0 6px, #fff 0) top left,
    linear-gradient(-135deg, transparent 0 0px, #fff 0) top right;
  mask-size: 50.5% 50.5%;
  mask-repeat: no-repeat;
`;

const ContentContainer = styled.div`
  display: flex;
  z-index: 1;
  padding: 2px;
  padding-right: 10px;
  color: white;

  div{
    &:nth-child(2) {
      flex: 1;
      text-align: left;
    }
  }

  img {
    max-height: 21.6px;
    margin-top: 2px;
    margin-right: 3px;
  }
`;

/*
  const SimpleFlex = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-around;
    align-items: center;
  `;

  const getBattleCommandContent = (command: BattleCommand) => (
    <div>
      <strong>{command.name}</strong>
      <p>{command.description}</p>
      <SimpleFlex>
        <div>
          <strong>Max. Level:</strong>
          <br />
          <span>{command.max}</span>
        </div>
        <div>
          <strong>Damage Type:</strong>
          <br />
          <span>{command.damageType}</span>
        </div>
        <div>
          <strong>Class:</strong>
          <br />
          <span>{command.class}</span>
        </div>
        <div>
          <strong>Slots:</strong>
          <br />
          <span>{command.slots}</span>
        </div>
      </SimpleFlex>
    </div>
  );

  const getActionCommandContent = (command: ActionCommand) => (
    <div>
      <strong>{command.name}</strong>
      <p>{command.description}</p>
      <SimpleFlex>
        <div>
          <strong>Max. Level:</strong>
          <br />
          <span>{command.max}</span>
        </div>
      </SimpleFlex>
    </div>
  );

  const getShotlockCommandContent = (command: ShotlockCommand) => (
    <div>
      <strong>{command.name}</strong>
      <p>{command.description}</p>
      <SimpleFlex>
        <div>
          <strong>Max. Level:</strong>
          <br />
          <span>{command.max}</span>
        </div>
        <div>
          <strong>Damage Type:</strong>
          <br />
          <span>{command.damageType}</span>
        </div>
        <div>
          <strong>Max. Shots:</strong>
          <br />
          <span>{command.shots}</span>
        </div>
      </SimpleFlex>
    </div>
  );
*/

interface FancyCommandProps {
  children?: React.ReactNode;
  command: Command;
  onClick?: () => void;
  // tooltipEnabled?: boolean;
}

const FancyCommand = ({ children, command, onClick }: FancyCommandProps) => (
  <Container>
    <ContentContainer color={COMMAND_COLOURS[command.type]} onClick={onClick}>
      <div>
        <CommandSymbol type={command.type} />
      </div>
      <div>
        <span><Trans id={command.name} /></span>
      </div>
      <div>
        {children}
      </div>
    </ContentContainer>

    <Background color={COMMAND_COLOURS[command.type]} />
  </Container>
);

FancyCommand.defaultProps = {
  children: null,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  onClick: () => {},
  // tooltipEnabled: false,
};

export default FancyCommand;
