import { useCallback } from 'react';
import styled, { css } from 'styled-components';
import { Trans } from '@lingui/macro';

import type { Command, Crystal, Recipe } from '../database';
import { abilities, crystals, recipeTypes } from '../database';
import { useAppDispatch, useAppSelector } from '../hooks';
import { updateMeldCrystal, selectPendingAbilityCounts, selectActiveCharacter } from '../reducer';

import FancyCommand from './FancyCommand';

const Container = styled.div`
  display: flex;
  flex-direction: column;

  > div {
    display: flex;
    align-items: center;
  }
`;

const RecipesContainer = styled.div`
  flex-direction: column;
`;

interface RecipeTableProps {
  clickable: boolean;
}

const RecipeTable = styled.table<RecipeTableProps>`
  border: 1px solid black;
  width: 100%;

  ${(props) => props.clickable ? css`
    cursor: pointer;

    &:hover {
      background-color: #ddd;
      border: 2px solid black;
    }
  ` : ''}

  td {
    border: 1px solid black;

    &:first-child {
      width: 75%;
    }
  }
`;

interface CommandRecipesProps {
  command: Command;
  crystal?: Crystal;
  index: number;
  onCloseClick?: (index: number) => ((() => void) | undefined);
  onRecipeClick?: (recipe: Recipe) => ((() => void) | undefined);
  recipes: Recipe[];
  queued?: boolean;
}

const NO_CLICK = () => undefined;

const CommandRecipes = ({
  command,
  crystal,
  index,
  onCloseClick,
  onRecipeClick,
  recipes,
  queued,
}: CommandRecipesProps) => {
  const dispatch = useAppDispatch();
  const activeCharacter = useAppSelector(selectActiveCharacter);
  const pendingAbilityCounts = useAppSelector(selectPendingAbilityCounts);

  const updateCrystal = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLSelectElement>) => dispatch(updateMeldCrystal({
      index,
      crystal: crystals[value as Crystal] as Crystal,
    })),
    [dispatch, index],
  );

  return (
    <Container key={command.name}>
      <div>
        <FancyCommand command={command}>
          {onCloseClick && (
            <span onClick={onCloseClick(index)}>X</span>
          )}
        </FancyCommand>
      </div>
      <RecipesContainer>
        {recipes.filter((recipe) => recipe.availableFor[activeCharacter]).map((recipe) => (
          <RecipeTable
            key={recipe.ingredients.toString()}
            clickable={Boolean(onRecipeClick)}
            onClick={onRecipeClick?.(recipe)}
          >
            <tbody>
              <tr>
                <td>
                  <Trans id={recipe.ingredients[0]} />
                </td>
                <td rowSpan={2}>
                  {recipe.type === '-' ? `${recipe['%']}%` : (
                    <>
                      {recipe.type}
                      <br />
                      {recipe['%'] !== 100 && <small>({recipe['%']}%)</small>}
                    </>
                  )}
                </td>
              </tr>
              <tr>
                <td>
                  <Trans id={recipe.ingredients[1]} />
                </td>
              </tr>
              {(queued && recipe.type !== '-') && (
                <tr>
                  <td colSpan={3}>
                    <select onChange={updateCrystal} value={crystal ?? ''}>
                      <option value=""><Trans>Select Crystal</Trans></option>
                      {(Object.values(crystals) as Crystal[]).map((crystalType) => {
                        const ability = recipeTypes[recipe.type][crystalType];
                        const maxed = Boolean(ability &&
                          abilities[ability].max === pendingAbilityCounts[ability]);

                        return (
                          <option
                            key={crystalType}
                            disabled={maxed}
                            value={crystalType}
                          >
                            <Trans id={crystalType} /> - <Trans id={ability} />
                          </option>
                        );
                      })}
                    </select>
                  </td>
                </tr>
              )}
            </tbody>
          </RecipeTable>
        ))}
      </RecipesContainer>
    </Container>
  );
};

CommandRecipes.defaultProps = {
  crystal: null,
  onCloseClick: NO_CLICK,
  onRecipeClick: NO_CLICK,
  queued: false,
};

export default CommandRecipes;
