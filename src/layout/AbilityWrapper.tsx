import styled, { css } from 'styled-components';
import { Trans } from '@lingui/macro';

import type { AbilityType } from '../database';
import { abilityTypes } from '../database';

import AbilityMarker from './AbilityMarker';

interface ContainerProps {
  isLabel?: boolean;
  type: AbilityType;
}

const Container = styled.div<ContainerProps>`
  display: flex;
  background: linear-gradient(#333, #aaa);
  padding: 3px;
  color: white;
  border-radius: 20px;
  flex-flow: row wrap;
  height: 32px;
  margin: 2px;
  position: relative;

  > div {
    width: 50%;
    display: flex;
  }

  ${(props) => props.isLabel ? css`
    mask:
      linear-gradient(45deg, transparent 0 0px , #fff 0) bottom left,
      linear-gradient(-45deg, transparent 0 11px, #fff 0) bottom right,
      linear-gradient(135deg, transparent 0 0px, #fff 0) top left,
      linear-gradient(-135deg, transparent 0 11px, #fff 0) top right;
    mask-size: 50.5% 50.5%;
    mask-repeat: no-repeat;
  ` : ''}
`;

const ChildrenContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: right;
`;

interface LineProps {
  type: AbilityType;
}

const LINE_COLOURS = {
  [abilityTypes.Prizes]: '#cbbd36',
  [abilityTypes.Stats]: '#2e91d2',
  [abilityTypes.Support]: '#bc0a08',
};
const LINE_OFFSET = 9;
const Line = styled.div<LineProps>`
  width: calc(100% - ${2 * LINE_OFFSET}px) !important;
  height: 2px;
  background-color: ${(props) => LINE_COLOURS[props.type]};
  position: absolute;
  left: ${LINE_OFFSET}px;
  bottom: 3px;
  z-index: 1;
`;

interface AbilityWrapperProps {
  children?: React.ReactNode;
  isLabel?: boolean;
  onClick?: () => void;
  text: string;
  type: AbilityType;
}

const AbilityWrapper = ({ children, onClick, isLabel, text, type }: AbilityWrapperProps) => (
  <Container isLabel={isLabel} type={type}>
    <div onClick={onClick}>
      <AbilityMarker type={type} />
      <span><Trans id={text} /></span>
    </div>

    <ChildrenContainer>
      {children}
    </ChildrenContainer>

    <Line type={type} />
  </Container>
);

AbilityWrapper.defaultProps = {
  children: null,
  isLabel: false,
  onClick: () => {
    // nothing
  },
};


export default AbilityWrapper;
