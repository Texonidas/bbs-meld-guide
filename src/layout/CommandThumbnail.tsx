/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable no-magic-numbers */
import { useCallback } from 'react';
import styled, { css } from 'styled-components';
import Tooltip from 'react-simple-tooltip';
import { darken, lighten } from 'polished';
import { i18n } from '@lingui/core';

import { useAppDispatch } from '../hooks';
import type { Command, CommandType } from '../database';
import { commandTypes } from '../database';
import { toggleCommandDiscovered } from '../reducer';

import CommandSymbol from './CommandSymbol';

const COMMAND_COLOURS = {
  [commandTypes.Attack]: '#aa4c1d',
  [commandTypes.Magic]: '#653688',
  [commandTypes.Movement]: '#886800',
  [commandTypes.Defence]: '#000072',
  [commandTypes.Reprisal]: '#680000',
  [commandTypes.Shotlock]: '#007200',
};

interface ContainerProps {
  active: boolean;
  type: CommandType;
}

const LIGHT_TOP = {
  [commandTypes.Attack]: true,
  [commandTypes.Magic]: true,
} as { [key in CommandType]: boolean };

const getActiveGradient = (type: CommandType) => css`
  background: linear-gradient(
    ${darken(LIGHT_TOP[type] ? 0.2 : 0.7, COMMAND_COLOURS[type])} ,
    ${lighten(LIGHT_TOP[type] ? 0.2 : 0.10, COMMAND_COLOURS[type])}
  );
`;

const Container = styled.div<ContainerProps>`
  margin: 5px;
  padding: 2px;
  height: 40px;
  width: 40px;
  cursor: pointer;
  ${(props) => props.active ? getActiveGradient(props.type) : css`
    filter: grayscale(100%);
    background: #555;
  `}

  img {
    height: 36px;
    width: 36px;
  }
`;

interface CommandThumbnailProps {
  command: Command;
  active: boolean;
}

const CommandThumbnail = ({ command, active }: CommandThumbnailProps) => {
  const dispatch = useAppDispatch();
  const toggleDiscovered = useCallback(
    () => dispatch(toggleCommandDiscovered(command.name)),
    [dispatch, command.name],
  );

  const tooltipCSS = css`
    white-space: nowrap;
  `;

  return (
    <Tooltip
      content={i18n._(command.name)}
      customCss={tooltipCSS}
      padding={5}
    >
      <Container active={active} onClick={toggleDiscovered} type={command.type}>
        <CommandSymbol type={command.type} />
      </Container>
    </Tooltip>
  );
};

export default CommandThumbnail;
