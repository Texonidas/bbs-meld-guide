import './index.scss';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { HashRouter } from 'react-router-dom';
import { Provider as StoreProvider } from 'react-redux';

import I18nApp from './I18nApp';
import store from './store';

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById('root')!).render((
  <React.StrictMode>
    <HashRouter>
      <StoreProvider store={store}>
        <I18nApp />
      </StoreProvider>
    </HashRouter>
  </React.StrictMode>
));
