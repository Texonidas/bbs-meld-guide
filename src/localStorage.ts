/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import type { RootState } from './store';

const updateStateShape = [
  (state: Record<string, unknown>) => {
    const { inventories, ...newState } = state;

    return inventories ? { ...newState, inventory: inventories } : newState;
  },
  (state: Record<string, unknown>) => {
    const { beforeDisableFeedback, ...newState } = state;

    if (beforeDisableFeedback === undefined) {
      return {
        ...newState,
        beforeDisableFeedback: true,
      };
    }

    return state;
  },
];

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    const parsed = JSON.parse(serializedState
      .replaceAll('terra', 'Terra')
      .replaceAll('ventus', 'Ventus')
      .replaceAll('aqua', 'Aqua')) as Record<string, unknown>;

    return updateStateShape.reduce((transformed, update) => update(transformed), parsed);
  }
  catch (err) {
    return undefined;
  }
};

export const saveState = (state: RootState) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  }
  catch {
    // ignore write errors
  }
};
