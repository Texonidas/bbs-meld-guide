import { useCallback, useState } from 'react';

const useToggle = (initialState: boolean) => {
  const [state, setState] = useState(initialState);

  const toggle = useCallback(() => setState((current) => !current), []);

  return [state, toggle, setState] as [boolean, () => void, typeof setState];
};

export default useToggle;
