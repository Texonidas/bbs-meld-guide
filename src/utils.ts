export const toSlug = (str = '') => str.toLowerCase().replace(/\s/gu, '-');

export const toCapitalCase = (str = '') => str.replace('-', ' ').split(' ')
  .map((word) => `${word.charAt(0).toUpperCase()}${word.slice(1)}`)
  .join(' ');

export const unique = <T, >(arr: T[] = []): T[] => arr
  .filter((entry, index, self) => self.indexOf(entry) === index);
