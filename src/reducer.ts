/* eslint-disable sort-keys */
import { createAction, createReducer, createSelector } from '@reduxjs/toolkit';

import { characters, abilities, commands, recipes, recipeTypes } from './database';
import type { Character, Recipe, Crystal } from './database';

import type { RootState } from './store';

type CountableState = Record<string, number>;

type ToggleableState = Record<string, boolean>;

export interface Meld {
  crystal?: Crystal;
  recipe: Recipe;
}

const initialAbilityCounts: CountableState = {
  'Zero EXP': 1,
  'Scan': 1,
};

export const initialState = {
  abilities: {
    [characters.Terra]: initialAbilityCounts,
    [characters.Ventus]: initialAbilityCounts,
    [characters.Aqua]: initialAbilityCounts,
  } as { [key in Character]: CountableState },
  character: characters.Terra as Character,
  discovered: {
    [characters.Terra]: {} as ToggleableState,
    [characters.Ventus]: {} as ToggleableState,
    [characters.Aqua]: {} as ToggleableState,
  } as { [key in Character]: ToggleableState },
  inventory: {
    [characters.Terra]: {} as CountableState,
    [characters.Ventus]: {} as CountableState,
    [characters.Aqua]: {} as CountableState,
  } as { [key in Character]: CountableState },
  queuedMelds: {
    [characters.Terra]: [] as Meld[],
    [characters.Ventus]: [] as Meld[],
    [characters.Aqua]: [] as Meld[],
  } as { [key in Character]: Meld[] },
  recipeBook: {
    [characters.Terra]: {} as ToggleableState,
    [characters.Ventus]: {} as ToggleableState,
    [characters.Aqua]: {} as ToggleableState,
  } as { [key in Character]: ToggleableState },
  shoppingList: {
    [characters.Terra]: [] as Recipe[],
    [characters.Ventus]: [] as Recipe[],
    [characters.Aqua]: [] as Recipe[],
  } as { [key in Character]: Recipe[] },
  beforeDisableFeedback: false,
  feedbackModalPops: 0,
  lastFeedbackModalPop: 0,
  totalSessionTime: 0,
};

export const isSelfMeld = (recipe: Recipe) => recipe.ingredients[0] === recipe.ingredients[1];

export const isMeldable = (
  character: Character,
  inventory: CountableState,
  used: CountableState,
) => (recipe: Recipe) => recipe.availableFor[character] && recipe.ingredients
  .reduce((bothPresent: boolean, ingredient) => {
    const remaining = (inventory[ingredient] || 0) - (used[ingredient] || 0);

    // check if there's at least 2 for self melds
    return bothPresent && remaining > Number(isSelfMeld(recipe));
  }, true);

interface InventoryUpdate {
  command: string;
  newValue: number;
}

interface MeldUpdate {
  index: number;
  crystal?: Crystal;
}

export const updateSessionTime = createAction<number>('UPDATE_SESSION_TIME');
export const addFeedbackModalPop = createAction('ADD_FEEDBACK_MODAL_POP');

export const chooseCharacter = createAction<Character>('CHOOSE_CHARACTERS');

export const incrementAbility = createAction<string>('INCREMENT_ABILITY');
export const decrementAbility = createAction<string>('DECREMENT_ABILITY');

export const decrementInventoryCommand = createAction<string>('DECREMENT_INVENTORY_COMMAND');
export const updateInventoryCommand = createAction<InventoryUpdate>('UPDATE_INVENTORY_COMMAND');
export const incrementInventoryCommand = createAction<string>('INCREMENT_INVENTORY_COMMAND');

export const queueMeld = createAction<Recipe>('QUEUE_MELD');
export const updateMeldCrystal = createAction<MeldUpdate>('ADD_MELD_CRYSTAL');
export const removeQueuedMeld = createAction<number>('REMOVE_QUEUED_MELD');
export const executeQueuedMelds = createAction('EXECUTE_QUEUED_MELDS');

export const discoverCommand = createAction<string>('DISCOVER_COMMAND');
export const undiscoverCommand = createAction<string>('UNDISCOVER_COMMAND');
export const toggleCommandDiscovered = createAction<string>('TOGGLE_COMMAND_DISCOVERED');

export const toggleRecipeBook = createAction<string>('TOGGLE_RECIPE_BOOK');

export const queueShoppingRecipe = createAction<Recipe>('QUEUE_SHOPPING_RECIPE');
export const removeQueuedShoppingRecipe = createAction<number>('REMOVE_QUEUED_SHOPPING_RECIPE');
export const goShopping = createAction('GO_SHOPPING');

const reducer = createReducer(
  initialState,
  (builder) => builder
    .addCase(updateSessionTime, (state, action) => ({
      ...state,
      totalSessionTime: state.totalSessionTime + action.payload,
    }))
    .addCase(addFeedbackModalPop, (state) => ({
      ...state,
      feedbackModalPops: state.feedbackModalPops + 1,
      lastFeedbackModalPop: state.totalSessionTime,
    }))
    .addCase(chooseCharacter, (state, action) => ({
      ...state,
      character: action.payload,
    }))

    .addCase(incrementAbility, (state, action) => ({
      ...state,
      abilities: {
        ...state.abilities,
        [state.character]: {
          ...state.abilities[state.character],
          [action.payload]: (state.abilities[state.character][action.payload] || 0) + 1,
        },
      },
    }))
    .addCase(decrementAbility, (state, action) => ({
      ...state,
      abilities: {
        ...state.abilities,
        [state.character]: {
          ...state.abilities[state.character],
          [action.payload]: (state.abilities[state.character][action.payload] || 0) - 1,
        },
      },
    }))

    .addCase(decrementInventoryCommand, (state, action) => ({
      ...state,
      inventory: {
        ...state.inventory,
        [state.character]: {
          ...state.inventory[state.character],
          [action.payload]: (state.inventory[state.character][action.payload] || 0) - 1,
        },
      },
    }))
    .addCase(updateInventoryCommand, (state, action) => ({
      ...state,
      inventory: {
        ...state.inventory,
        [state.character]: {
          ...state.inventory[state.character],
          [action.payload.command]: action.payload.newValue,
        },
      },
    }))
    .addCase(incrementInventoryCommand, (state, action) => ({
      ...state,
      inventory: {
        ...state.inventory,
        [state.character]: {
          ...state.inventory[state.character],
          [action.payload]: (state.inventory[state.character][action.payload] || 0) + 1,
        },
      },
    }))

    .addCase(queueMeld, (state, action) => ({
      ...state,
      queuedMelds: {
        ...state.queuedMelds,
        [state.character]: [...state.queuedMelds[state.character], { recipe: action.payload }],
      },
    }))
    .addCase(updateMeldCrystal, (state, action) => ({
      ...state,
      queuedMelds: {
        ...state.queuedMelds,
        [state.character]: state.queuedMelds[state.character]
          .map((meld, inner) => inner === action.payload.index ? {
            ...meld,
            crystal: action.payload.crystal,
          } : meld),
      },
    }))
    .addCase(removeQueuedMeld, (state, action) => ({
      ...state,
      queuedMelds: {
        ...state.queuedMelds,
        [state.character]: state.queuedMelds[state.character]
          .filter((_, index) => index !== action.payload),
      },
    }))
    .addCase(executeQueuedMelds, (state) => ({
      ...state,
      abilities: {
        ...state.abilities,
        [state.character]: state.queuedMelds[state.character]
          .reduce((acc, { recipe, crystal }) => {
            const ability = crystal ? recipeTypes[recipe.type][crystal] : null;

            return ability ? {
              ...acc,
              [ability]: Math.min(
                (acc[ability] || 0) + 1,
                abilities[ability].max,
              ),
            } : acc;
          }, state.abilities[state.character]),
      },
      discovered: {
        ...state.discovered,
        [state.character]: state.queuedMelds[state.character]
          .reduce((acc, { recipe }) => ({
            ...acc,
            [recipe.result]: true,
          }), state.discovered[state.character]),
      },
      inventory: {
        ...state.inventory,
        [state.character]: state.queuedMelds[state.character]
          .reduce((acc, { recipe: { result, ingredients: [first, second] } }) => ({
            ...acc,
            [first]: (acc[first] || 0) - 1,
            [result]: (acc[result] || 0) + 1,
            [second]: (acc[second] || 0) - (first === second ? 2 : 1),
          }), state.inventory[state.character]),
      },
      queuedMelds: {
        ...state.queuedMelds,
        [state.character]: [],
      },
    }))

    .addCase(discoverCommand, (state, action) => ({
      ...state,
      discovered: {
        ...state.discovered,
        [state.character]: {
          ...state.discovered[state.character],
          [action.payload]: true,
        },
      },
    }))
    .addCase(undiscoverCommand, (state, action) => ({
      ...state,
      discovered: {
        ...state.discovered,
        [state.character]: {
          ...state.discovered[state.character],
          [action.payload]: false,
        },
      },
    }))
    .addCase(toggleCommandDiscovered, (state, action) => ({
      ...state,
      discovered: {
        ...state.discovered,
        [state.character]: {
          ...state.discovered[state.character],
          [action.payload]: !state.discovered[state.character][action.payload],
        },
      },
    }))

    .addCase(toggleRecipeBook, (state, action) => ({
      ...state,
      recipeBook: {
        ...state.recipeBook,
        [state.character]: {
          ...state.recipeBook[state.character],
          [action.payload]: !state.recipeBook[state.character][action.payload],
        },
      },
    }))

    .addCase(queueShoppingRecipe, (state, action) => ({
      ...state,
      shoppingList: {
        ...state.shoppingList,
        [state.character]: [...state.shoppingList[state.character], action.payload],
      },
    }))
    .addCase(removeQueuedShoppingRecipe, (state, action) => ({
      ...state,
      shoppingList: {
        ...state.shoppingList,
        [state.character]: state.shoppingList[state.character]
          .filter((_, index) => index !== action.payload),
      },
    }))
    .addCase(goShopping, (state) => ({
      ...state,
      discovered: {
        ...state.discovered,
        [state.character]: state.shoppingList[state.character]
          .reduce((acc, recipe) => ({
            ...acc,
            [recipe.ingredients[0]]: true,
            [recipe.ingredients[1]]: true,
          }), state.discovered[state.character]),
      },
      inventory: {
        ...state.inventory,
        [state.character]: state.shoppingList[state.character]
          .reduce((acc, { ingredients: [first, second] }) => ({
            ...acc,
            [first]: (acc[first] || 0) + 1,
            [second]: (acc[second] || 0) + (first === second ? 2 : 1),
          }), state.inventory[state.character]),
      },
      queuedMelds: {
        ...state.queuedMelds,
        [state.character]: [
          ...state.queuedMelds[state.character],
          ...state.shoppingList[state.character].map((recipe) => ({ recipe })),
        ],
      },
      shoppingList: {
        ...state.shoppingList,
        [state.character]: [],
      },
    }))
);

const MINUTES = 60 * 1000;
const FEEDBACK_MODAL_POP_INTERVAL = 120;
export const selectFeedbackModalReady = (state: RootState) => {
  const nextPopTime = state.lastFeedbackModalPop + (FEEDBACK_MODAL_POP_INTERVAL * MINUTES);

  return state.totalSessionTime > nextPopTime;
};
export const selectBeforeDisableFeedback = (state: RootState) => state.beforeDisableFeedback;

export const selectSessionTotalMinutes = (state: RootState) => state.totalSessionTime / MINUTES;
export const selectActiveCharacter = (state: RootState) => state.character;
export const selectAbilityCounts = (state: RootState) => state.abilities[state.character];
export const selectDiscoveredCommands = (state: RootState) => state.discovered[state.character];
export const selectInventory = (state: RootState) => state.inventory[state.character];
export const selectQueuedMelds = (state: RootState) => state.queuedMelds[state.character];
export const selectRecipeBook = (state: RootState) => state.recipeBook[state.character];

export const selectUsedCommands = createSelector(
  selectQueuedMelds,
  (queuedMelds) => queuedMelds
    .reduce<string[]>((acc, { recipe }) => [...acc, ...recipe.ingredients], [])
    .reduce<CountableState>((acc, ingredient) => ({
      ...acc,
      [ingredient]: (acc[ingredient] || 0) + 1,
    }), {}),
);
export const selectMeldableCommands = createSelector(
  selectActiveCharacter,
  selectInventory,
  selectUsedCommands,
  (character, inventory, used) => Object.values(commands)
    .reduce<string[]>((acc, command) => {
      const availableRecipes = (recipes[command.name] ?? [])
        .filter(isMeldable(character, inventory, used));

      return availableRecipes.length ? [...acc, command.name] : acc;
    }, []),
);
export const selectPendingAbilityCounts = createSelector(
  selectAbilityCounts,
  selectQueuedMelds,
  (currentAbilities, queuedMelds) => queuedMelds.reduce((acc, { recipe, crystal }) => {
    const ability = crystal ? recipeTypes[recipe.type][crystal] : null;

    return ability ? {
      ...acc,
      [ability]: Math.min(
        (currentAbilities[ability] || 0) + 1,
        abilities[ability].max,
      ),
    } : acc;
  }, currentAbilities),
);
export const selectUnmaxedAbilities = createSelector(
  selectPendingAbilityCounts,
  (counts) => Object.keys(abilities)
    .filter((ability) => (counts[ability] || 0) < abilities[ability].max),
);

export default reducer;
