import { useCallback, useState } from 'react';

const useUpdate = <T, >(initialState: T) => {
  const [state, setState] = useState<T>(initialState);
  const updateState = useCallback((newState: T) => () => setState(newState), []);

  return [state, updateState] as [T, typeof updateState];
};

export default useUpdate;
