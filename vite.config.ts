import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import macrosPlugin from 'vite-plugin-babel-macros';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    macrosPlugin(),
    react({
      babel: {
        presets: ['@babel/preset-react', '@babel/preset-typescript'],
        plugins: [
          'macros',
          'babel-plugin-styled-components',
          '@babel/plugin-syntax-dynamic-import',
          '@babel/plugin-proposal-class-properties',
        ],
      },
    }),
  ],
});
