## Sources:
Descriptions/Stats:
  - https://www.khwiki.com/Abilities_(KHBBS)
  - https://kingdomhearts.fandom.com/wiki/Deck_Command
  - https://kingdomhearts.fandom.com/wiki/Shotlock

Game Assets:
  - https://github.com/AkiraJkr/Birth-by-Sleep-HD-ReMix

Recipe List:
  - https://www.finalfantasykingdom.net/bbsmelding.php

### Tools:
Character Image to Letter (Recipe List):
```js
['terra', 'ventus', 'aqua']
  .forEach((name) => $$(`img[src$="bbs/dl${name}1.png"]`)
    .forEach((element) => element.parentElement
      .replaceChild(document.createTextNode(name[0].toUpperCase()), element)
    )
  );
```
PNG manipulation:
  - https://pixlr.com/x/#editor
