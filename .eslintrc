{
    "root": true,
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "project": "./tsconfig.json"
    },
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    "globals": {
        "angular": "readonly",
        "ENV_CONFIG": "readonly"
    },
    "settings": {
        "react": {
            "version": "detect"
        }
    },
    "plugins": [
        "@typescript-eslint",
        "no-loops",
        "react-hooks"
    ],
    "extends": [
        "eslint:all",
        "plugin:react/all",
        "plugin:@typescript-eslint/all",
        "plugin:react/jsx-runtime"
    ],
    "rules": {
        // to review
        "array-element-newline": "off",
        "max-lines-per-function": "off",
        "lines-between-class-members": "off",
        "sort-imports": "off",
        "no-confusing-arrow": "off",
        "react/jsx-one-expression-per-line": "off", // [2, {"allow": "single-child"}]

        // eslint
        "@typescript-eslint/brace-style": ["error", "stroustrup"],
        "capitalized-comments": ["warn", "never", {
            "block": {
                "ignorePattern": "."
            }
        }],
        "@typescript-eslint/comma-dangle": ["warn", "always-multiline"],
        "consistent-return": "warn",
        "consistent-this": ["error", "vm"],
        "dot-location": ["error", "property"],
        "func-names": "off",
        "func-style": "off",
        "function-call-argument-newline": ["warn", "consistent"],
        "id-length": ["warn", {
            "exceptions": ["a", "b", "i", "o", "Q", "_"]
        }],
        "@typescript-eslint/indent": ["warn", 2],
        "init-declarations": "off",
        "line-comment-position": "off",
        "linebreak-style": "off",
        "max-len": ["warn", {
            "code": 100,
            "ignoreComments": true,
            "ignoreStrings": true,
        }],
        "max-lines": "off",
        "max-statements": "off",
        "max-params": "off",
        "multiline-comment-style": "off",
        "multiline-ternary": "off",
        "no-console": "warn",
        "@typescript-eslint/no-extra-parens": "off",
        "no-inline-comments": "off",
        "no-invalid-this": 0,
        "@typescript-eslint/no-magic-numbers": ["error", {
            "ignore": [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 24, 60, 100, 365, 1000],
            "ignoreArrayIndexes": true
        }],
        "no-param-reassign": "error",
        "no-plusplus": "off",
        "no-prototype-builtins": "off",
        "no-ternary": "off",
        "no-use-before-define": ["error", {
            "functions": false,
            "classes": true
        }],
        "no-undefined": "off",
        "no-unused-expressions": ["error", {
            "allowShortCircuit": true,
            "allowTernary": true
        }],
        "@typescript-eslint/no-unused-vars": ["error", { "ignoreRestSiblings": true }],
        "no-var": "off",
        "@typescript-eslint/object-curly-spacing": ["warn", "always"],
        "object-curly-newline": ["error", {
            "ObjectExpression": { "multiline": true, "consistent": true },
            "ObjectPattern": { "multiline": true, "consistent": true },
            "ImportDeclaration": { "multiline": true, "consistent": true }
        }],
        "object-property-newline": "off",
        "object-shorthand": ["error", "always"], // update when switching to es6
        "one-var": ["warn", "never"],
        "padded-blocks": "off",
        "prefer-arrow-callback": "warn", // update when switching to es6
        "prefer-destructuring": "warn", // update when switching to es6
        "prefer-spread": "warn", // update when switching to es6
        "prefer-template": "warn", // update when switching to es6
        "require-jsdoc": "off",
        "quote-props": "off",
        "quotes": ["error", "single"],
        "sort-keys": ["warn", "asc", { "minKeys": 4 }],
        "@typescript-eslint/space-before-function-paren": "off",
        "spaced-comment": ["error", "always", {
            "exceptions": ["/"],
            "markers": ["/", "!"]
        }],
        "valid-jsdoc": "off",
        "vars-on-top": "off",
        "wrap-iife": "off",

        // plugin-react
        "react/function-component-definition": ["error", {
            "namedComponents": "arrow-function",
            "unnamedComponents": "arrow-function"
        }],
        "react/no-unescaped-entities": ["error", {"forbid": [">", "}"]}],
        "react/prop-types": "off",

        // plugin-no-loops
        "no-loops/no-loops": 2,

        // plugin-react/jsx
        "react/jsx-filename-extension": "off",
        "react/jsx-indent": [2, 2, {"checkAttributes": true}],
        "react/jsx-indent-props": [1, 2],
        "react/jsx-max-depth": "off",
        "react/jsx-max-props-per-line": [1, {"maximum": 3}],
        "react/jsx-newline": "off",
        "react/jsx-no-literals": "off",
        "react/jsx-handler-names": "off",
        "react/jsx-sort-props": ["warn", {
            "ignoreCase": true,
            "noSortAlphabetically": true,
            "reservedFirst": true
        }],

        // plugin-react-hooks
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "error",

        // typescript
        "@typescript-eslint/await-thenable": "off",
        "@typescript-eslint/quotes": ["error", "single"],
        "@typescript-eslint/naming-convention": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-floating-promises": "off",
        "@typescript-eslint/prefer-readonly-parameter-types": "off",
        "@typescript-eslint/no-confusing-void-expression": [
            "error",
            { "ignoreArrowShorthand": true }
        ],
        "@typescript-eslint/strict-boolean-expressions": "off",
        "@typescript-eslint/no-type-alias": "off"
    }
}
